package com.example.myconductorapp.controllers;

import android.content.Context;
import android.content.Intent;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myconductorapp.model.DataStorage;
import com.example.myconductorapp.model.DbManager;
import com.example.myconductorapp.model.entities.Conductor;
import com.example.myconductorapp.views.AuthActivity;

import com.example.myconductorapp.R;
import com.example.myconductorapp.views.MainActivity;

public class ControllerAuthActivity
{
    private AuthActivity authActivity;
    private DbManager db;

    public ControllerAuthActivity(AuthActivity authActivity)
    {
        this.authActivity = authActivity;
        db = DbManager.getInstance(this.authActivity.getApplicationContext());
    }

    public void Authorize()
    {
        EditText editTextLogin, editTextPassword;
        Context context;

        editTextLogin = authActivity.findViewById(R.id.editTextLogin);
        editTextPassword = authActivity.findViewById(R.id.editTextPassword);
        context = authActivity.getApplicationContext();

        String login = editTextLogin.getText().toString();
        String password = editTextPassword.getText().toString();

        if(login.equals("")==true)
        {
            Toast.makeText(context,"Error. Login is empty",Toast.LENGTH_SHORT).show();
            return;
        }

        if(password.equals("")==true)
        {
            Toast.makeText(context,"Error. Password is empty",Toast.LENGTH_SHORT).show();
            return;
        }

        Conductor conductor = db.getTableConductors().getByLoginAndPassword(login, password);

        if(conductor==null)
        {
            Toast.makeText(context,"Error. Login or Password is wrong",Toast.LENGTH_SHORT).show();
        }
        else
        {
            editTextLogin.getText().clear();
            editTextPassword.getText().clear();

            DataStorage.Add("conductor", conductor);

            Intent intent = new Intent(context, MainActivity.class);
            authActivity.startActivity(intent);
        }

    }

    public void Exit()
    {
        authActivity.finish();
    }


}
