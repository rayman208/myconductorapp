package com.example.myconductorapp.controllers;

import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import com.example.myconductorapp.R;
import com.example.myconductorapp.model.DataStorage;
import com.example.myconductorapp.model.DbManager;
import com.example.myconductorapp.model.entities.Conductor;
import com.example.myconductorapp.model.entities.Passanger;
import com.example.myconductorapp.views.MainActivity;
import com.example.myconductorapp.views.main_activity_tools.PassangersAdapter;

import java.util.ArrayList;

public class ControllerMainActivity
{
    private MainActivity mainActivity;
    private DbManager db;

    private ArrayList<Passanger> passangers;

    public ControllerMainActivity(MainActivity mainActivity)
    {
        this.mainActivity = mainActivity;

        db = DbManager.getInstance(this.mainActivity.getApplicationContext());

        passangers = db.getTablePassengers().getAll();

        TextView textView = this.mainActivity.findViewById(R.id.textViewConductor);
        Conductor conductor = (Conductor) DataStorage.Get("conductor");

        textView.setText("Welcome, "+conductor.getName());
    }

    private void UpdateListViewPassangers(ArrayList<Passanger> passangers)
    {
        PassangersAdapter adapter = new PassangersAdapter(mainActivity.getApplicationContext(), passangers);

        ListView listView = mainActivity.findViewById(R.id.listViewPassangers);

        listView.setAdapter(adapter);
    }

    public void UpdateListViewPassangersWithoutFilter()
    {
        UpdateListViewPassangers(passangers);
    }

    public void UpdateListViewPassangersWithFilter()
    {
        ArrayList<Passanger> filterPassangers = new ArrayList<>();

        EditText editText = mainActivity.findViewById(R.id.editTextPassport);
        String passportPart = editText.getText().toString();

        for (int i=0;i<passangers.size();i++)
        {
            if(passangers.get(i).getPassport().startsWith(passportPart)==true)
            {
                filterPassangers.add(passangers.get(i));
            }
        }

        UpdateListViewPassangers(filterPassangers);
    }

    public void ReloadPassangers()
    {
        passangers = db.getTablePassengers().getAll();
    }

    public void LogOut()
    {
        DataStorage.Delete("conductor");
        mainActivity.finish();
    }
}
