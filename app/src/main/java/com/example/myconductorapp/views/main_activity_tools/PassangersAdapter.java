package com.example.myconductorapp.views.main_activity_tools;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.myconductorapp.model.entities.Passanger;

import java.util.ArrayList;

public class PassangersAdapter extends BaseAdapter
{
    private ArrayList<Passanger> passangers;

    Context context;
    LayoutInflater layoutInflater;

    public PassangersAdapter(Context context, ArrayList<Passanger> passangers)
    {
        this.context = context;
        this.passangers = passangers;

        layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public int getCount() {
        return passangers.size();
    }

    @Override
    public Object getItem(int i) {
        return passangers.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = view;

        if(v == null)
        {
            v = layoutInflater.inflate(android.R.layout.simple_list_item_1, viewGroup, false);
        }

        Passanger passanger = passangers.get(i);

        TextView textView = v.findViewById(android.R.id.text1);

        String str = String.format("FIO: %s %s\nPassport: %s",passanger.getLastName(), passanger.getFirstName(), passanger.getPassport());

        textView.setText(str);

        return v;
    }
}
