package com.example.myconductorapp.views;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myconductorapp.R;
import com.example.myconductorapp.controllers.ControllerMainActivity;

public class MainActivity extends AppCompatActivity {

    private ControllerMainActivity controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        controller = new ControllerMainActivity(this);
        controller.UpdateListViewPassangersWithoutFilter();

        EditText editTextPassport = findViewById(R.id.editTextPassport);
        editTextPassport.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2)
            {
                controller.UpdateListViewPassangersWithFilter();
            }

            @Override
            public void afterTextChanged(Editable editable) { }
        });
    }

    public void onButtonReloadPassangersClick(View view)
    {
        controller.ReloadPassangers();
        controller.UpdateListViewPassangersWithoutFilter();

        Toast.makeText(getApplicationContext(),"Passangers successfull reloaded",Toast.LENGTH_SHORT).show();
    }

    public void onButtonLogOutClick(View view)
    {
        controller.LogOut();
    }
}
