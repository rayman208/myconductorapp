package com.example.myconductorapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myconductorapp.model.entities.Conductor;
import com.example.myconductorapp.model.tools.DbHelper;

public class TableConductors
{
    private DbHelper dbHelper;

    public TableConductors(DbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
    }

    public Conductor getByLoginAndPassword(String login, String password)
    {
        Conductor conductor = null;

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = String.format("SELECT * FROM `conductors` WHERE `login`='%s' AND `password`='%s'", login, password);

        Cursor cursor = db.rawQuery(sqlCommand, null);

        if(cursor.moveToFirst()==true)
        {
            conductor = new Conductor(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );
        }

        cursor.close();

        dbHelper.close();

        return conductor;
    }
}
