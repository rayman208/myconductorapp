package com.example.myconductorapp.model;

import android.content.Context;

import com.example.myconductorapp.model.tables.TableConductors;
import com.example.myconductorapp.model.tables.TablePassengers;
import com.example.myconductorapp.model.tools.DbHelper;

public class DbManager
{
    private static DbManager instance = null;

    public static DbManager getInstance(Context context)
    {
        if(instance == null)
        {
            instance = new DbManager(context);
        }
        return instance;
    }

    private TableConductors tableConductors;
    private TablePassengers tablePassengers;

    private DbManager(Context context)
    {
        DbHelper dbHelper = new DbHelper(context);

        tableConductors = new TableConductors(dbHelper);
        tablePassengers = new TablePassengers(dbHelper);
    }

    public TableConductors getTableConductors() {
        return tableConductors;
    }

    public TablePassengers getTablePassengers() {
        return tablePassengers;
    }
}
