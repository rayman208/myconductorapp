package com.example.myconductorapp.model.entities;

public class Passanger
{
    private int id;
    private String lastName;
    private String firstName;
    private String passport;

    public Passanger(int id, String lastName, String firstName, String passport)
    {
        this.id = id;
        this.lastName = lastName;
        this.firstName = firstName;
        this.passport = passport;
    }

    public int getId() {
        return id;
    }

    public String getLastName() {
        return lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getPassport() {
        return passport;
    }
}
