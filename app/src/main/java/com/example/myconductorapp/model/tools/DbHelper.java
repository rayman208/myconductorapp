package com.example.myconductorapp.model.tools;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class DbHelper extends SQLiteOpenHelper
{
    public DbHelper(Context context)
    {
        super(context, "app.db", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        db.execSQL("CREATE TABLE IF NOT EXISTS \"conductors\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"name\"\tTEXT NOT NULL,\n" +
                "\t\"login\"\tTEXT NOT NULL,\n" +
                "\t\"password\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("CREATE TABLE IF NOT EXISTS \"passengers\" (\n" +
                "\t\"id\"\tINTEGER NOT NULL,\n" +
                "\t\"last_name\"\tTEXT NOT NULL,\n" +
                "\t\"first_name\"\tTEXT NOT NULL,\n" +
                "\t\"passport\"\tTEXT NOT NULL,\n" +
                "\tPRIMARY KEY(\"id\" AUTOINCREMENT)\n" +
                ");");

        db.execSQL("DELETE FROM conductors");
        db.execSQL("DELETE FROM passengers");

        db.execSQL("INSERT INTO \"conductors\" (\"id\",\"name\",\"login\",\"password\") VALUES (1,'Иван','ivan','123'),\n" +
                " (2,'Владимир','vlad','234');");

        db.execSQL("INSERT INTO \"passengers\" (\"id\",\"last_name\",\"first_name\",\"passport\") VALUES (1,'Иванов','Пётр','1521 123321'),\n" +
                " (2,'Максимов','Константин','1518 435892'),\n" +
                " (3,'Сильнов','Владимир','1584 238742'),\n" +
                " (4,'Сысоев','Владислав','1581 345322');");

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }
}
