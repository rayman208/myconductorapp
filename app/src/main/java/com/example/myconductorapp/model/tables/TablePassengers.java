package com.example.myconductorapp.model.tables;

import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.myconductorapp.model.entities.Passanger;
import com.example.myconductorapp.model.tools.DbHelper;

import java.util.ArrayList;

public class TablePassengers
{
    private DbHelper dbHelper;

    public TablePassengers(DbHelper dbHelper)
    {
        this.dbHelper = dbHelper;
    }

    public ArrayList<Passanger> getAll()
    {
        ArrayList<Passanger> passangers = new ArrayList<>();

        SQLiteDatabase db = dbHelper.getWritableDatabase();

        String sqlCommand = "SELECT * FROM `passengers`";

        Cursor cursor = db.rawQuery(sqlCommand, null);

        while (cursor.moveToNext() == true)
        {
            Passanger passanger = new Passanger(
                    cursor.getInt(0),
                    cursor.getString(1),
                    cursor.getString(2),
                    cursor.getString(3)
            );

            passangers.add(passanger);
        }

        cursor.close();

        dbHelper.close();

        return passangers;
    }
}
